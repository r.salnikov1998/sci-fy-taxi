﻿using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class UIMoney : MonoBehaviour
{
    [SerializeField] private Player _player;
    [SerializeField] private float _animationDuration;

    private TMP_Text _text;
    private float _displayedMoney;

    private void Awake()
    {
        _text = GetComponent<TMP_Text>();
    }

    private void OnEnable()
    {
        _player.MoneyChanged += OnMoneyChanged;
    }

    private void OnDisable()
    {
        _player.MoneyChanged -= OnMoneyChanged;
    }

    private void OnMoneyChanged(float money)
    {
        DOTween.To(x => _displayedMoney = x, _displayedMoney, money, _animationDuration).OnUpdate(() => _text.text = _displayedMoney.ToString("0"));
    }


}
