﻿using DG.Tweening;
using UnityEngine;

[CreateAssetMenu]
public class IconAnimationSO : ScriptableObject
{
    public Ease easeType;

    public float offsetY;
    public float scale;
    public float fade;

    public float moveDuration;
    public float rotationDuration;
    public float scaleDuration;
    public float fadeDuration;
}
