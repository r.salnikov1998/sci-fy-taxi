﻿using DG.Tweening;
using UnityEngine;

public abstract class AbstractIcon : MonoBehaviour
{
    [SerializeField] private IconAnimationSO _iconSO;

    private Tweener tween;

    private void Start()
    {
        tween = transform.DOMoveY(2, 1).SetRelative().SetLoops(-1, LoopType.Yoyo);
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.TryGetComponent(out Player player))
        {
            AnimateIcon();
        }
    }

    private void AnimateIcon()
    {
        KillTween();

        Sequence sequnce = DOTween.Sequence();

        Ease easeType = _iconSO.easeType;

        float offsetY = _iconSO.offsetY;
        float scale = _iconSO.scale;
        float fade = _iconSO.fade;

        float moveDuration = _iconSO.moveDuration;
        float rotationDuration = _iconSO.rotationDuration;
        float scaleDuration = _iconSO.scaleDuration;
        float fadeDuration = _iconSO.fadeDuration;

        tween = transform.DOMoveY(offsetY, moveDuration).SetEase(easeType).SetRelative().SetLoops(-1, LoopType.Yoyo);
        tween = transform.DORotate(new Vector3(0, 180, 0), rotationDuration).SetEase(easeType).SetLoops(-1, LoopType.Incremental);
        tween = transform.DOScale(scale, scaleDuration).SetLoops(-1, LoopType.Yoyo);
        tween = GetComponentInChildren<MeshRenderer>().material.DOFade(fade, fadeDuration).OnComplete(KillTween);
    }

    private void KillTween()
    {
        DOTween.Kill(transform);
    }

}
