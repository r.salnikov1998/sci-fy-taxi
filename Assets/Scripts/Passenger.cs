﻿using UnityEngine;
using DG.Tweening;

public class Passenger : MonoBehaviour
{
    private enum DestinationDirection
    {
        Up,
        Left
    }

    [SerializeField] private DestinationDirection _destinationDirection;
    [SerializeField] private float _moveDuration;
    [SerializeField] private Player _player;
    [SerializeField] private ParticleSystem _poofPrefab;
    [SerializeField] private ParticleSystem _smilePrefab;

    private Animator _animator;
    private int _isWalkingHash;

    private void Awake()
    {
        _animator = GetComponent<Animator>();
        _isWalkingHash = Animator.StringToHash("isWalking");
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.TryGetComponent(out Player player))
        {
            SpawnPoofEffect();
            Deactivate();
        }
    }

    private void IdleAnimation()
    {
        Debug.Log("Should idle animation");
        _animator.SetBool(_isWalkingHash, false);
    }

    private void SpawnPoofEffect()
    {
        var spawned = Instantiate(_poofPrefab, transform.position + (Vector3.up * 3), Quaternion.identity);
    }

    private void SpawnSmileEffect()
    {
        Debug.Log("Should smile effect");
        var spawned = Instantiate(_smilePrefab, transform.position + (Vector3.up * 6), Quaternion.identity);
    }

    public void MoveToPlayer()
    {
        _animator.SetBool(_isWalkingHash, true);
        Vector3 playerPosition = _player.transform.position;
        var positionToMove = new Vector3(playerPosition.x, transform.position.y, playerPosition.z);
        transform.DOMove(positionToMove, _moveDuration);
    }

    public void MoveToDestination()
    {
        _animator.SetBool(_isWalkingHash, true);
        transform.position = _player.transform.position;
        float distanceToGo = 5;
        Vector3 destinationPoint;

        SpawnPoofEffect();

        Sequence sequence = DOTween.Sequence();

        switch (_destinationDirection)
        {
            case DestinationDirection.Up:
                destinationPoint = transform.position + Vector3.forward * distanceToGo;

                sequence.Append(transform.DOLookAt(destinationPoint, 0));
                sequence.Insert(0, transform.DOMoveZ(distanceToGo, _moveDuration).SetRelative().OnComplete(SpawnSmileEffect));
                sequence.OnComplete(IdleAnimation);
                sequence.Append(transform.DOLookAt(_player.transform.position, 0));
                break;

            case DestinationDirection.Left:
                destinationPoint = transform.position + Vector3.left * distanceToGo;

                sequence.Append(transform.DOLookAt(destinationPoint, 0));
                sequence.Insert(0, transform.DOMoveX(-distanceToGo, _moveDuration).SetRelative().OnComplete(SpawnSmileEffect));
                sequence.OnComplete(IdleAnimation);
                sequence.Append(transform.DOLookAt(_player.transform.position, 0));
                break;
        }
    }

    public void LookAtPlayer()
    {
        transform.DOLookAt(_player.transform.position, 0);
    }

    public void Activate()
    {
        gameObject.SetActive(true);
    }

    public void Deactivate()
    {
        gameObject.SetActive(false);
    }

}
